Feature: Movie Awards
  I want my telegram bot to respond to the command /awards movie_title

  Scenario: a user sends a message with the command and a movie title with awards
    When the user sends the message "/awards Titanic" to the bot
    Then the bot answers with "Won 11 Oscars. 126 wins & 83 nominations total"

  Scenario: a user sends a message with the command and a movie title without awards
    When the user sends the message "/awards Emoji" to the bot
    Then the bot answers with "The movie Emoji has no awards"

  Scenario: a user sends a message with the command and a movie title with only nominations
    When the user sends the message "/awards Heat" to the bot
    Then the bot answers with "15 nominations"

  Scenario: a user sends a message with the command and a movie title that doesn't exist
    When the user sends the message "/awards this movie doesn't exist" to the bot
    Then the bot answers with "The movie 'this movie doesn't exist' doesn't exist"

  Scenario: a user sends a message with the command and multiple movie titles
    When the user sends the message "/awards Titanic; Heat, Emoji" to the bot
    Then the bot answers with "Won 11 Oscars. 126 wins & 83 nominations total"
    And the bot answers with "has 15 nominations"
    And the bot answers with "The movie Emoji has no awards"