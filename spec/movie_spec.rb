require 'spec_helper'
require "#{File.dirname(__FILE__)}/../app/movie/movie"

describe 'Movie' do
  it 'a movie with won awards won something' do
    movie = Movie::Movie.new('Titanic',
                             'Won 11 Oscars. 126 wins & 83 nominations total')
    expect(movie.won_anything?).to be true
  end

  it 'a movie with no awards or nominations has nothing' do
    movie = Movie::Movie.new('Titanic',
                             'N/A')
    expect(movie.has_nothing?).to be true
  end

  it 'a movie with no title raises an exception' do
    expect { Movie::Movie.new('', '') }.to raise_error(Movie::MovieWithoutTitleError)
  end
end
