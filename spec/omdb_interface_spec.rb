require 'spec_helper'
require "#{File.dirname(__FILE__)}/../app/api_interface/omdb_interface"

describe 'omdb_interface' do
  let(:titanic_info) do
    body = {
      'Title' => 'Titanic',
      'Awards' => 'Won 11 Oscars. 126 wins & 83 nominations total'
    }.to_json

    body
  end

  it 'when asked for the movie Titanic, it should return the title titanic' do
    mock_object = instance_double('OmdbApi')
    allow(mock_object).to receive(:get).with('Titanic').and_return(titanic_info)
    api = OmdbInterface.new(mock_object)
    movie = api.get('Titanic')
    expect(movie.title).to eq 'Titanic'
  end

  it 'when asked for the movie Titanic, it should return the awards for titanic' do
    mock_object = instance_double('OmdbApi')
    allow(mock_object).to receive(:get).with('Titanic').and_return(titanic_info)
    api = OmdbInterface.new(mock_object)
    movie = api.get('Titanic')
    expect(movie.awards).to eq 'Won 11 Oscars. 126 wins & 83 nominations total'
  end
end
