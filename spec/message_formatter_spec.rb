require 'spec_helper'
require "#{File.dirname(__FILE__)}/../app/messages/message_formatter"
require "#{File.dirname(__FILE__)}/../app/movie/movie"

describe 'Message Formatter' do
  let(:movie_with_awards) do
    movie = Movie::Movie.new('Titanic', 'Won 11 Oscars. 126 wins & 83 nominations total')
    movie
  end
  let(:movie_with_no_awards) do
    movie = Movie::Movie.new('Emoji', 'N/A')
    movie
  end

  let(:movie_with_nominations) do
    movie = Movie::Movie.new('Heat', '15 nominations')
    movie
  end

  it 'message for a /awards for a movie with awards' do
    formatter = Message::MessageFormatter.new(movie_with_awards)
    message = 'Won 11 Oscars. 126 wins & 83 nominations total'
    expect(formatter.message.strip).to eq message
  end

  it 'message for a /awards for a movie with no awards or nominations' do
    formatter = Message::MessageFormatter.new(movie_with_no_awards)
    message = 'The movie Emoji has no awards'
    expect(formatter.message.strip).to eq message
  end

  it 'message for a /awards for a movie with only nominations' do
    formatter = Message::MessageFormatter.new(movie_with_nominations)
    message = '15 nominations'
    expect(formatter.message.strip).to eq message
  end
end
