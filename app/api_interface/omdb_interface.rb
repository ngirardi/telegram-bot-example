require_relative '../movie/movie'
class OmdbInterface
  def initialize(api)
    @api = api
  end

  def get(title)
    json = @api.get(title)
    answer = JSON.parse(json)
    Movie::Movie.new(answer['Title'], answer['Awards'])
  end
end
