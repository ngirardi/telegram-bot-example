module Message
  class MessageFormatter
    def initialize(movie)
      @movie = movie
    end

    def message
      return "The movie #{@movie.title} has no awards" if @movie.has_nothing?

      @movie.awards
    end
  end
end
