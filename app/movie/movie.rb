module Movie
  class MovieWithoutTitleError < RuntimeError
  end

  class Movie
    attr :title, :awards

    def initialize(title, awards)
      @title = title
      @awards = awards
      raise MovieWithoutTitleError, 'no title specified' unless valid?
    end

    def won_anything?
      @awards.downcase.include?('won')
    end

    def has_nothing?
      @awards.downcase.include?('n/a')
    end

    protected

    def valid?
      @title != '' and !@title.nil?
    end
  end
end
