class OmdBApi
  def initialize(url, key)
    @url = url
    @key = key
  end

  def get(title)
    response = Faraday.get(@url, { t: title, apikey: @key })
    response.body
  end
end
